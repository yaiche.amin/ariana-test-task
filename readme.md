# Installation
## Backend
- Run the following command: `pip install -r requirements.txt` inside the backend directory.
Note: This project support python 3 only.
- Create `config.json` file inside backend/settings directory containing the following information (in json format):
    - `secret_key`: containing the secret key of the app
    - `database_name`: The name of the database
    - `database_user`: The user of the database
    - `database_password`: The password of the database user
    - `database_host`: The database host
    - `database_port`: The port of the database
Note: The database must be postgreSQL as the models contain fields that are specific to postgreSQL. (JSONField and ArrayField).

## Frontend
- Install nodejs 6.* or later, then run `npm install` inside the frontend directory.
- Run `npm run` to start the project

# API documentation
The API contains Four endpoints:
- GET `/api/questionnaires/<pk>`: Used to fetch the next question of a given questionnaire. The format of the response is as follows:
    ```javascript
    {
        "question": "Are you hungry?",
        "choices": ["Yes", "No"],
        "comment": null
    }
    ```
- POST `/api/questionnaires/submit/<pk>`: Used to submit answer for a given questionnaire. The body of the request must have the following format:
    ```javascript
    {
        "answer": "Yes"
    }
    ```
-  POST `/api/questionnaires/reset/<pk>`: Used to reset a questionnaire from the beginning. The body of the request is empty
-  GET `/api/questionnaires`: Used to list the available questionnaires. The body of the response has the following format:
    ```javascript
    [
        {
            "id": 1,
            "name": "Food order questionnaire",
            "creation_date": "2018-12-19T18:00:00+01:00"
        }
    ]
    ```
 # Adding a questionnaire
 A questionnaire can be loaded to the server as a JSON file using the admin interface (questionnaire section). `example.json` contains an example of a questionnaire file.
