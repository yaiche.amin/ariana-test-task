from django.conf.urls import url

from questionnaires.views import QuestionnaireDetailView, QuestionnaireListView, SubmitQuestionnaireView, \
    ResetQuestionnaireView

urlpatterns = [
    url(r'^(?P<pk>\d+)$', QuestionnaireDetailView.as_view()),
    url(r'^submit/(?P<pk>\d+)$', SubmitQuestionnaireView.as_view()),
    url(r'^reset/(?P<pk>\d+)$', ResetQuestionnaireView.as_view()),
    url(r'^$', QuestionnaireListView.as_view({'get': 'list'})),
]
