from django.contrib import admin

from questionnaires.forms import QuestionnaireForm
from questionnaires.models import Questionnaire, OngoingQuestionnaire


class QuestionnairesAdmin(admin.ModelAdmin):
    list_display = ('name',)
    form = QuestionnaireForm


class OngoingQuestionnairesAdmin(admin.ModelAdmin):
    list_display = ('questionnaire', 'answers')


admin.site.register(Questionnaire, QuestionnairesAdmin)
admin.site.register(OngoingQuestionnaire, OngoingQuestionnairesAdmin)
