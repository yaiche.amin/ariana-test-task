from django.core.exceptions import ValidationError


def validate_question(question):
    for key in question:
        if key not in ['question', 'choices']:
            raise ValidationError('Unknown key `{}` in question section.'
                                  'Expecting ["question", "choices"].'.format(key))
        if 'question' not in question or 'choices' not in question:
            raise ValidationError('question and choices are required in the question section.')

        if not isinstance(question['question'], str):
            raise ValidationError('question value should be a string.')

        if not isinstance(question['choices'], list):
            raise ValidationError('choices must be a list.')

        for choice in question['choices']:
            validate_answer(choice)


def validate_answer(answer):
    for key in answer:
        if key not in ['answer', 'subquestion', 'comment']:
            raise ValidationError('Unknown key `{}` in answer section.'
                                  'Expecting ["answer", "subquestion", "comment"].'.format(key))
    if 'answer' not in answer:
        raise ValidationError('answer is required is answer section.')

    if len(answer.keys()) == 3:
        raise ValidationError('cannot put subquestion and commnet in the same answer section.')

    if not isinstance(answer['answer'], str):
        raise ValidationError('answer value should be a string.')

    if not isinstance(answer.get('comment', ''), str):
        raise ValidationError('comment value should be a string.')
    if not isinstance(answer.get('subquestion', {}), dict):
        raise ValidationError('subquestion section must be an object')

    if 'subquestion' in answer:
        validate_question(answer['subquestion'])
