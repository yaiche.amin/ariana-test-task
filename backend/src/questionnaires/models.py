from django.conf import settings
from django.contrib.postgres.fields import JSONField, ArrayField
from django.db import models
from django.utils.translation import gettext as _
from rest_framework.exceptions import ParseError

from questionnaires.validators import validate_question


class Questionnaire(models.Model):
    name = models.CharField(max_length=settings.QUESTIONNAIRE_NAME_MAX_LENGTH)
    questions = JSONField(validators=[validate_question])
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Questionnaire')
        verbose_name_plural = _('Questionnaires')

    @staticmethod
    def _lookup_next_step(questions, previous_answers):
        """
        Given the previously answered question, this function returns the next question to ask
        :param questions: A dict that contains a questionnaire or sub questionnaire
        :param previous_answers: A list that contains answers of previously asked questions
        :return: tuple of next question, next question's choices and next comment
        """
        if len(previous_answers) == 0:
            question = questions.get('question')
            comment = questions.get('comment')
            choices = map(lambda choice: choice['answer'], questions['choices'])
            return question, choices, comment
        for choice in questions['choices']:
            if choice['answer'] != previous_answers[0]:
                continue
            if 'subquestion' in choice:
                return Questionnaire._lookup_next_step(choice['subquestion'], previous_answers[1:])
            if len(previous_answers) > 1:
                raise ParseError('Unknown previous answer `{}`'.format(previous_answers[1]))
            return None, None, choice.get('comment')
        raise ParseError('Unknown previous answer `{}`'.format(previous_answers[0]))

    def get_next_step(self, previous_answers):
        return Questionnaire._lookup_next_step(self.questions, previous_answers)


class OngoingQuestionnaire(models.Model):
    answers = ArrayField(models.CharField(max_length=settings.ANSWER_MAX_LENGTH), size=5)
    questionnaire = models.OneToOneField(Questionnaire, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Ongoing questionnaire')
        verbose_name_plural = _('Ongoing questionnaires')

    @classmethod
    def add_answer(cls, answer, questionnaire):
        """
        Save answer to the database
        :param answer: str
        :param questionnaire: a model entity of the concerned questionnaire
        :return: OngoingQuestionnaire model
        """
        queryset = OngoingQuestionnaire.objects.filter(questionnaire=questionnaire)
        if queryset.exists():
            ongoing_questionnaire = queryset.first()
            ongoing_questionnaire.answers.append(answer)
            ongoing_questionnaire.save()
            return ongoing_questionnaire
        return OngoingQuestionnaire.objects.create(questionnaire=questionnaire, answers=[answer])

    def conversation_finished(self, questions=None, index=0):
        """
        Determines whether a conversation is finished or not.
        :param questions: a dict that represents a questionnaire
        :param index: int, used to parse the questions dict recursively
        :return: bool, whether the conversation is finished or not
        """
        questions = questions or self.questionnaire.questions
        if len(self.answers) == index:
            return False

        for choice in questions['choices']:
            if choice['answer'] != self.answers[index]:
                continue
            if 'subquestion' in choice:
                return self.conversation_finished(choice['subquestion'], index + 1)
            return len(self.answers) == index + 1
        return False

    def log_questionnaire(self):
        """
        Log the questionnaire into the stdout
        """
        print('{}: {}'.format(self.questionnaire.questions['question'], ' -> '.join(self.answers)))
