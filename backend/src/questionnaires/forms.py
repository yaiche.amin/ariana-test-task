from django.forms import ModelForm, FileInput

from questionnaires.models import Questionnaire


class QuestionsInput(FileInput):
    def value_from_datadict(self, data, files, name):
        return files.get('questions').read()


class QuestionnaireForm(ModelForm):
    class Meta:
        model = Questionnaire
        widgets = {
            "questions": QuestionsInput
        }
        fields = '__all__'
