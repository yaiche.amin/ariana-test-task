# Generated by Django 2.1.4 on 2018-12-15 12:54

import django.contrib.postgres.fields.jsonb
from django.db import migrations
import questionnaires.models


class Migration(migrations.Migration):

    dependencies = [
        ('questionnaires', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questionnaire',
            name='questions',
            field=django.contrib.postgres.fields.jsonb.JSONField(validators=[questionnaires.models.validate_question]),
        ),
    ]
