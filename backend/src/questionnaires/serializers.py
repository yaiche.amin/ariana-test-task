from rest_framework import serializers

from questionnaires.models import Questionnaire


class QuestionnaireListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questionnaire
        fields = ('id', 'name', 'creation_date')


class SubmitQuestionnaire(serializers.Serializer):
    answer = serializers.CharField()


class ResetQuestionnaire(serializers.Serializer):
    pass