from http import HTTPStatus

from rest_framework.exceptions import ParseError
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import  GenericViewSet

from questionnaires.models import Questionnaire, OngoingQuestionnaire
from questionnaires.serializers import QuestionnaireListSerializer, SubmitQuestionnaire, ResetQuestionnaire


class QuestionnaireListView(ListModelMixin, GenericViewSet):
    queryset = Questionnaire.objects.all().order_by('-creation_date')
    serializer_class = QuestionnaireListSerializer


class QuestionnaireDetailView(GenericAPIView):
    def get(self, request, *args, **kwargs):
        """
        Returns the next question to ask
        """
        questionnaire_id = kwargs.get('pk')
        try:
            questionnaire = Questionnaire.objects.get(id=questionnaire_id)
        except Questionnaire.DoesNotExist:
            raise ParseError('Questionnaire having id {} does not exist'.format(questionnaire_id))
        ongoing_questionnaire = OngoingQuestionnaire.objects.filter(questionnaire=questionnaire)

        if ongoing_questionnaire.exists():
            previous_answers = ongoing_questionnaire.first().answers
        else:
            previous_answers = []

        next_question, choices, comment = questionnaire.get_next_step(previous_answers)
        is_first = len(previous_answers) == 0
        return Response({"question": next_question, "choices": choices, "comment": comment, "is_first": is_first})

    @classmethod
    def get_extra_actions(cls):
        return []


class SubmitQuestionnaireView(GenericAPIView):
    serializer_class = SubmitQuestionnaire

    def post(self, request, *args, **kwargs):
        """
        Submit answer
        """
        answer = request.data.get('answer')
        questionnaire_id = kwargs.get('pk')
        try:
            questionnaire = Questionnaire.objects.get(id=questionnaire_id)
        except Questionnaire.DoesNotExist:
            raise ParseError('Questionnaire having id {} does not exist'.format(questionnaire_id))

        ongoing_questionnaire = OngoingQuestionnaire.add_answer(answer, questionnaire)

        if ongoing_questionnaire.conversation_finished():
            ongoing_questionnaire.log_questionnaire()

        return Response(status=HTTPStatus.CREATED)


class ResetQuestionnaireView(GenericAPIView):
    serializer_class = ResetQuestionnaire
    def post(self, request, *args, **kwargs):
        """
        Reset a questionnaire from the beginning
        """
        questionnaire_id = kwargs.get('pk')
        try:
            questionnaire = Questionnaire.objects.get(id=questionnaire_id)
        except Questionnaire.DoesNotExist:
            raise ParseError('Questionnaire having id {} does not exist'.format(questionnaire_id))

        ongoing_questionnaire = OngoingQuestionnaire.objects.filter(questionnaire=questionnaire)
        if not ongoing_questionnaire.exists():
            return ParseError('No ongoing questionnaire')
        ongoing_questionnaire.delete()
        return Response()
