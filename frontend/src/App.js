import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import { CookiesProvider } from 'react-cookie';

import QuestionnaireList from './QuestionnairesList';
import QuestionnaireDetail from './QuestionnaireDetail';


class App extends Component {
  render() {
    return (
      <Router>
        <CookiesProvider>
          <Route path='/questionnaire/:id' component={QuestionnaireDetail} />
          <Route path='/' exact component={QuestionnaireList} />
        </CookiesProvider>
      </Router>
    )
  }
}

export default App;
