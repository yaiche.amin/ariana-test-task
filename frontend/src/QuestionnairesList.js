import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

class QuestionnairesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questionnaires: []
    }
  }
  componentDidMount() {
    fetch('/api/questionnaires')
      .then(response => response.json())
      .then((json) => this.setState({questionnaires: json}))
  }
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <h2>Questionnaires list</h2>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Questionnaire</TableCell>
              <TableCell align="right">Creation date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.questionnaires.map(row => {
              return (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    <a href={"questionnaire/" + row.id}>{row.name}</a>
                  </TableCell>
                  <TableCell align="right">{row.creation_date}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}


QuestionnairesList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(QuestionnairesList);