import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import cookie from 'react-cookies'
import Button from '@material-ui/core/Button';


const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit * 3,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});

class RadioButtonsGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.handleGetBackClick = this.handleGetBackClick.bind(this)
    this.handleReset = this.handleReset.bind(this)
  }
  state = {
    value: 'female',
  };

  handleChange = event => {
    fetch('/api/questionnaires/submit/' + this.props.match.params.id , {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRFToken': cookie.load('csrftoken')
      },
      body: JSON.stringify({answer: event.target.value})
    }).then(this.getNextQuestion)
  };

  componentDidMount() {
    this.getNextQuestion()
  }

  getNextQuestion = () => {
    fetch('/api/questionnaires/' + this.props.match.params.id)
    .then(response => response.json())
    .then((json) => this.setState({questionnaire: json}))
  }

  handleGetBackClick(){
    this.props.history.push('/')
  }

  handleReset() {
    fetch('/api/questionnaires/reset/' + this.props.match.params.id, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRFToken': cookie.load('csrftoken')
      },
    }).then(this.getNextQuestion)
  }

  render() {
    const { classes } = this.props;
    if(!this.state.questionnaire ) return <div>Loading ...</div>
    return (
      <div>
        <Button variant="contained" color="primary" onClick={this.handleGetBackClick}>Get back</Button>&nbsp;
        <Button variant="contained" color="primary" onClick={this.handleReset}>Reset</Button><br/>
        <div className={classes.root} style={{"margin-top": "50px"}}>
          {this.state.questionnaire.question &&
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">{this.state.questionnaire.question}</FormLabel>
              <RadioGroup
                aria-label="Gender"
                name="gender1"
                className={classes.group}
                value={this.state.value}
                onChange={this.handleChange}
              >
              
                {this.state.questionnaire.choices.map((choice, index) => {
                  return (
                    <FormControlLabel key={index} value={choice} control={<Radio />} label={choice} />
                  )
                })

                }
              </RadioGroup>
            </FormControl>
          }
          {
            this.state.questionnaire.comment &&
            <FormControl>
              <FormLabel>{this.state.questionnaire.comment}</FormLabel>
            </FormControl>
          }
        </div>
      </div>
    );
  }
}

RadioButtonsGroup.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RadioButtonsGroup);
